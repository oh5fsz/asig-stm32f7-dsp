/*
 * system.c
 *
 * Copyright (c) 2018 Toni Naukkarinen <toni.naukkarinen@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <communicator.h>
#include <defines.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include "process_audio.h"
#include "externs.h"
#include "receiver.h"
#include "audio_buffers.h"
#include "debug_dac.h"
#include "stm32f7xx_hal.h"
#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"
#include "queue.h"
#include "semphr.h"
#include "knobs.h"

TaskHandle_t system_task_handle;
EventGroupHandle_t system_event_group;

receiver_t* adc_receiver;
receiver_t* bt_receiver;

uint32_t notify_value;
uint32_t event_group_value;

// System IDLE task. Takes care of transmitting UART data etc.
void system_task(void *pvParams)
{
	system_event_group = xEventGroupCreate();
	xEventGroupSetBits(system_event_group, DAC_ENABLE_BIT);
	xTaskCreate(dac_process_task, "DACTask", 128, 0, 6, &dac_task_handle);
	xTaskCreate(communicator_task, "Communicator", 128, 0, 3, &communicator_task_handle);
	xTaskCreate(process_audio_task, "AudioProcess", 128, 0, 5, &process_audio_task_handle);

	// Start bluetooth module
	HAL_GPIO_WritePin(BLUETOOTH_RX_IND_GPIO_Port, BLUETOOTH_RX_IND_Pin, GPIO_PIN_SET);
	adc_receiver = new_receiver(&hsai_BlockA1, (uint32_t*)*ADC_RX_BUFFER);
	adc_receiver->start(adc_receiver);

	HAL_TIM_Base_Start_IT(&htim2);
	init_knobs();
	bt_receiver = new_receiver(&hsai_BlockB1, (uint32_t*)*BT_RX_BUFFER);
	// bt_receiver->start(bt_receiver);
	vTaskDelete(system_task_handle);
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
}
