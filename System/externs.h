/*
 * externs.h
 *
 * Copyright (c) 2018 Toni Naukkarinen <toni.naukkarinen@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef EXTERNS_H_
#define EXTERNS_H_

#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"
#include "knobs.h"
#include "filter.h"
#include "output.h"

// Input SAI
extern SAI_HandleTypeDef hsai_BlockA1;
extern SAI_HandleTypeDef hsai_BlockB1;


// Output SAI
extern SAI_HandleTypeDef hsai_BlockA2;
extern SAI_HandleTypeDef hsai_BlockB2;

// UART
extern UART_HandleTypeDef huart1;

// Timers
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim6;

// DAC
extern DAC_HandleTypeDef hdac;

// ADC
extern ADC_HandleTypeDef hadc1;

// OS related
extern EventGroupHandle_t system_event_group;
extern QueueHandle_t uart_rx_queue;
extern QueueHandle_t uart_tx_queue;

extern TaskHandle_t system_task_handle;
extern TaskHandle_t dac_task_handle;
extern TaskHandle_t process_audio_task_handle;
extern TaskHandle_t communicator_task_handle;

// Knobs
extern knob_t* knob_array[3];

// Filters
extern filter_t* shelving_bass;
extern filter_t* shelving_treble;

extern filter_t* equalizer[5];

// I2C
extern I2C_HandleTypeDef hi2c1;


// Outputs
extern output_t* output_low;
extern output_t* output_high;
#endif /* EXTERNS_H_ */
