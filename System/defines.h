/*
 * common.h
 *
 * Copyright (c) 2018 Toni Naukkarinen <toni.naukkarinen@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef DEFINES_H_
#define DEFINES_H_

// Control knobs
#define TREBLE_CONTROL  0
#define BASS_CONTROL    1
#define VOLUME_CONTROL  2

// Helper defines for buffer sides
#define BUFFER_SIDE_A   0
#define BUFFER_SIDE_B   1

// Event group bits
#define DAC_ENABLE_BIT          (1 << 1)
#define UPDATE_TREBLE_FILTER    (1 << 2)
#define UPDATE_BASS_FILTER      (1 << 3)
#define UPDATE_EQ_CH_1          (1 << 4)
#define UPDATE_EQ_CH_2          (1 << 5)
#define UPDATE_EQ_CH_3          (1 << 6)
#define UPDATE_EQ_CH_4          (1 << 7)
#define UPDATE_EQ_CH_5          (1 << 8)
#define DISABLE_FILTER_UPDATE   (1 << 9)
#define USE_5_BAND_EQ           (1 << 10)
#define DISABLE_SHELVING_BASS   (1 << 11)
#define DISABLE_SHELVING_TREBLE (1 << 12)

// Used with task notifications to indicate which buffer side to process.
#define PROCESS_SIDE_A          (1 << 10)
#define PROCESS_SIDE_B          (1 << 11)
#define FILTER_UPDATE_REQUESTED (1 << 9)

#endif /* DEFINES_H_ */
