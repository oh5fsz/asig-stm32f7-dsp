/*
 * shell.h
 *
 * Copyright (c) 2018 Toni Naukkarinen <toni.naukkarinen@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef COMMUNICATOR_H_
#define COMMUNICATOR_H_

#include <stdint.h>

#define UART_QUEUE_LENGTH       64
#define PACKET_MAX_LENGTH       64
#define PACKET_START_BYTE       0x7E
#define PACKET_ESCAPE_BYTE      0x7D

typedef enum {
	PING = 0x06,
	ENABLE_DAC,
	DISABLE_DAC,
	ENABLE_CONTROLS,
	DISABLE_CONTROLS,
} uart_command_t;

typedef enum {
	PONG = 0x01,
	INVALID_CRC,
	FAIL,
	OK
} uart_response_t;

typedef enum {
	IDLE,
	DATA,
	ESCAPE
} rx_state_t;

typedef union {
	struct {
		uint8_t startbyte;
		uart_command_t command;
		uint8_t length;
		uint8_t payload[16];
		uint16_t crc;
	} data;
	uint8_t bytes[22];
} __attribute__((packed)) uart_packet_t;

typedef union {
	struct {
		uint8_t startbyte;
		uint8_t response;
		uint16_t crc;
	};
	uint8_t bytes[4];
} __attribute__((packed)) uart_response_packet_t;

void communicator_task(void *pvParams);

#endif /* COMMUNICATOR_H_ */
