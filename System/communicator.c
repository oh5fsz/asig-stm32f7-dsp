/*
 * shell.c
 *
 * Copyright (c) 2018 Toni Naukkarinen <toni.naukkarinen@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <defines.h>
#include <stdbool.h>
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "communicator.h"
#include "stm32f7xx_hal.h"
#include "crc.h"
#include "externs.h"
#include "filter.h"

#include <pb_encode.h>
#include <pb_decode.h>
#include "dsp-control.pb.h"

TaskHandle_t communicator_task_handle;
QueueHandle_t uart_rx_queue;
QueueHandle_t uart_tx_queue;
uint8_t rx_char;
uint32_t command_count = 0;

void HAL_UART_RxCpltCallback(UART_HandleTypeDef* huart)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	xQueueSendFromISR(uart_rx_queue, &rx_char, &xHigherPriorityTaskWoken);
	HAL_UART_Receive_IT(&huart1, &rx_char, 1);
	portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
}

static void transmit_response(uint8_t* buf, uint8_t length)
{
	// TODO: This could be done a bit smarter.
	uint8_t data_to_send[length + 4];
	uint16_t crc = 0xFFFF;
	data_to_send[0] = PACKET_START_BYTE;

	for (uint8_t i = 0; i < length; i++) {
		crc = update_crc(crc, buf[i]);
		if (buf[i] == 0x7E || buf[i] == 0x7D) {
			buf[i] = buf[i] ^ 0x20;
		}
	}
	memcpy(&data_to_send[1], buf, length);
	data_to_send[length + 1] = (uint8_t)crc;
	data_to_send[length + 2] = (uint8_t)(crc >> 8);
	data_to_send[length + 3] = PACKET_START_BYTE;
	HAL_UART_Transmit_IT(&huart1, data_to_send, length + 4);
}

static int command_handler(uint8_t *buf, uint8_t length, uint16_t calculated_crc, uint16_t received_crc) {
	// TODO: Check buffer size
	uint8_t outbuf[64];
	bool status;
	int rc;
	ControlPacket packet = ControlPacket_init_zero;
	ControlPacket tx_packet = ControlPacket_init_zero;

	pb_ostream_t ostream = pb_ostream_from_buffer(outbuf, sizeof(outbuf));
	pb_istream_t stream = pb_istream_from_buffer(buf, length);
	status = pb_decode(&stream, ControlPacket_fields, &packet);

//	if (received_crc != calculated_crc) {
//		tx_packet.command = packet.command;
//		tx_packet.response = ControlPacket_Response_CRC_MISMATCH;
//	}
	if (1 == 0) {

	}
	else {
		switch (packet.command) {
		tx_packet.command = packet.command;
		case ControlPacket_Command_PING:
			tx_packet.response = ControlPacket_Response_OK;
			break;
		case ControlPacket_Command_ENABLE_DAC:
			xEventGroupSetBits(system_event_group, DAC_ENABLE_BIT);
			tx_packet.response = ControlPacket_Response_OK;
			break;
		case ControlPacket_Command_DISABLE_DAC:
			xEventGroupClearBits(system_event_group, DAC_ENABLE_BIT);
			tx_packet.response = ControlPacket_Response_OK;
			break;
		case ControlPacket_Command_ENABLE_KNOBS:
			xEventGroupClearBits(system_event_group, DISABLE_FILTER_UPDATE);
			break;
		case ControlPacket_Command_DISABLE_KNOBS:
			xEventGroupSetBits(system_event_group, DISABLE_FILTER_UPDATE);
			break;
		case ControlPacket_Command_ENABLE_PARAMETRIC:
			xEventGroupSetBits(system_event_group, USE_5_BAND_EQ);
			break;
		case ControlPacket_Command_DISABLE_PARAMETRIC:
			xEventGroupClearBits(system_event_group, USE_5_BAND_EQ);
			break;
		case ControlPacket_Command_SET_EQ1_FC:
			rc = update_center_frequency(equalizer[0], packet.payload);
			xTaskNotify(process_audio_task_handle, FILTER_UPDATE_REQUESTED, eSetBits);
			break;
		case ControlPacket_Command_SET_EQ1_GAIN:
			rc = update_gain(equalizer[0], packet.payload);
			xTaskNotify(process_audio_task_handle, FILTER_UPDATE_REQUESTED, eSetBits);
			break;
		case ControlPacket_Command_SET_EQ2_FC:
			rc = update_center_frequency(equalizer[1], packet.payload);
			xTaskNotify(process_audio_task_handle, FILTER_UPDATE_REQUESTED, eSetBits);
			break;
		case ControlPacket_Command_SET_EQ2_GAIN:
			rc = update_gain(equalizer[1], packet.payload);
			xTaskNotify(process_audio_task_handle, FILTER_UPDATE_REQUESTED, eSetBits);
			break;
		case ControlPacket_Command_SET_EQ3_FC:
			rc = update_center_frequency(equalizer[2], packet.payload);
			xTaskNotify(process_audio_task_handle, FILTER_UPDATE_REQUESTED, eSetBits);
			break;
		case ControlPacket_Command_SET_EQ3_GAIN:
			rc = update_gain(equalizer[2], packet.payload);
			xTaskNotify(process_audio_task_handle, FILTER_UPDATE_REQUESTED, eSetBits);
			break;
		case ControlPacket_Command_SET_EQ4_FC:
			rc = update_center_frequency(equalizer[3], packet.payload);
			xTaskNotify(process_audio_task_handle, FILTER_UPDATE_REQUESTED, eSetBits);
			break;
		case ControlPacket_Command_SET_EQ4_GAIN:
			rc = update_gain(equalizer[3], packet.payload);
			xTaskNotify(process_audio_task_handle, FILTER_UPDATE_REQUESTED, eSetBits);
			break;
		case ControlPacket_Command_SET_EQ5_FC:
			rc = update_center_frequency(equalizer[4], packet.payload);
			xTaskNotify(process_audio_task_handle, FILTER_UPDATE_REQUESTED, eSetBits);
			break;
		case ControlPacket_Command_SET_EQ5_GAIN:
			rc = update_gain(equalizer[4], packet.payload);
			xTaskNotify(process_audio_task_handle, FILTER_UPDATE_REQUESTED, eSetBits);
			break;
		case ControlPacket_Command_GET_EQ1_FC:
		case ControlPacket_Command_GET_EQ2_FC:
		case ControlPacket_Command_GET_EQ3_FC:
		case ControlPacket_Command_GET_EQ4_FC:
		case ControlPacket_Command_GET_EQ5_FC:
		case ControlPacket_Command_GET_TREBLE_FC:
		case ControlPacket_Command_GET_TREBLE_GAIN:
		case ControlPacket_Command_GET_BASS_FC:
		case ControlPacket_Command_GET_BASS_GAIN:
		case ControlPacket_Command_SET_TREBLE_FC:
			rc = update_center_frequency(shelving_treble, packet.payload);
			tx_packet.response = rc;
			xTaskNotify(process_audio_task_handle, FILTER_UPDATE_REQUESTED, eSetBits);
			break;
		case ControlPacket_Command_SET_TREBLE_GAIN:
			rc = update_gain(shelving_treble, packet.payload);
			tx_packet.response = rc;
			xTaskNotify(process_audio_task_handle, FILTER_UPDATE_REQUESTED, eSetBits);
			break;
		case ControlPacket_Command_SET_BASS_FC:
			rc = update_center_frequency(shelving_bass, packet.payload);
			tx_packet.response = rc;
			xTaskNotify(process_audio_task_handle, FILTER_UPDATE_REQUESTED, eSetBits);
			break;
		case ControlPacket_Command_SET_BASS_GAIN:
			rc = update_gain(shelving_bass, packet.payload);
			tx_packet.response = rc;
			xTaskNotify(process_audio_task_handle, FILTER_UPDATE_REQUESTED, eSetBits);
			break;
		case ControlPacket_Command_ENABLE_SHELVING_TREBLE:
			xEventGroupClearBits(system_event_group, DISABLE_SHELVING_TREBLE);
			tx_packet.response = ControlPacket_Response_OK;
			break;
		case ControlPacket_Command_DISABLE_SHELVING_TREBLE:
			xEventGroupSetBits(system_event_group, DISABLE_SHELVING_TREBLE);
			tx_packet.response = ControlPacket_Response_OK;
			break;
		case ControlPacket_Command_ENABLE_SHELVING_BASS:
			xEventGroupClearBits(system_event_group, DISABLE_SHELVING_BASS);
			tx_packet.response = ControlPacket_Response_OK;
			break;
		case ControlPacket_Command_DISABLE_SHELVING_BASS:
			xEventGroupSetBits(system_event_group, DISABLE_SHELVING_BASS);
			tx_packet.response = ControlPacket_Response_OK;
			break;
		case ControlPacket_Command_GET_VOLUME:
		case ControlPacket_Command_SET_VOLUME:
			break;
		}
	}

	status = pb_encode(&ostream, ControlPacket_fields, &tx_packet);
	transmit_response(outbuf, ostream.bytes_written);
	return 0;
}

void communicator_task(void *pvParams)
{
	rx_state_t state = IDLE;
	uint8_t rx_buf;
	uint8_t uart_packet[PACKET_MAX_LENGTH];
	uint8_t rx_count = 0;
	uint16_t calculated_crc;
	uint16_t received_crc;

	HAL_UART_Receive_IT(&huart1, (uint8_t*)&rx_char, 1);

	for (;;) {
		if (xQueueReceive(uart_rx_queue, &rx_buf, 100) == pdTRUE) {
			switch (state) {
			case IDLE:
				if (rx_buf == PACKET_START_BYTE) {
					state = DATA;
					rx_count = 0;
				}
				break;
			case DATA:
				if (rx_buf == PACKET_ESCAPE_BYTE)
					state = ESCAPE;
				else if (rx_buf == PACKET_START_BYTE) {
					received_crc = ((uint16_t)uart_packet[rx_count-1] << 8) | uart_packet[rx_count - 2];
					calculated_crc = 0xFFFF;
					for (uint8_t i = 0; i < rx_count; i++) {
						calculated_crc = update_crc(calculated_crc, uart_packet[i]);
					}
					command_handler(uart_packet, rx_count-2, calculated_crc, received_crc);
					state = IDLE;
				}else  {
					uart_packet[rx_count] = rx_buf;
					rx_count++;
				}
				// If data length is over PACKET_MAX_LENGTH, abort.
				if (rx_count >= PACKET_MAX_LENGTH) {
					state = IDLE;
					rx_count = 0;
				}
				break;
			case ESCAPE:
				uart_packet[rx_count] = rx_buf ^ 0x20;
				state = DATA;
				rx_count++;
				break;
			}
		}
	}
}


