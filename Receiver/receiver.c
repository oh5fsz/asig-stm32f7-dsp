/*
 * receiver.c
 *
 * Copyright (c) 2018 Toni Naukkarinen <toni.naukkarinen@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "receiver.h"
#include "audio_buffers.h"
#include <assert.h>
#include <defines.h>
#include "FreeRTOS.h"
#include "task.h"
#include "output.h"

extern output_t* output_low;
extern output_t* output_high;

extern TaskHandle_t process_audio_task_handle;

static void start(receiver_t *self);
static void stop(receiver_t *self);

volatile uint32_t rx_isr_count;
uint32_t rx_process_count;
uint8_t oe = 0;

receiver_t* new_receiver(SAI_HandleTypeDef *sai_handle, uint32_t* buffer) {
	receiver_t* new_receiver = (receiver_t*)pvPortMalloc(sizeof(receiver_t));
	assert(new_receiver != NULL);
	new_receiver->sai_handle = sai_handle;
	new_receiver->buffer = buffer;

	new_receiver->start = &start;
	new_receiver->stop = &stop;
	return new_receiver;
}

static void start(receiver_t *self)
{
	HAL_SAI_Receive_DMA(self->sai_handle, (uint8_t*)self->buffer, (uint16_t)(AUDIO_BUFFER_LENGTH * AUDIO_BUFFER_SIDES));
}

static void stop(receiver_t *self)
{
	HAL_SAI_DMAStop(self->sai_handle);
}

// Half complete callback occurs when BUFFER_SIDE_A is filled. This means the first row of the array.
void HAL_SAI_RxHalfCpltCallback(SAI_HandleTypeDef *hsai) {
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	for (uint32_t i = 0; i < AUDIO_BUFFER_LENGTH; i++) {
		if (ADC_RX_BUFFER[BUFFER_SIDE_A][i] & 0x800000) {
			ADC_RX_BUFFER[BUFFER_SIDE_A][i] = 0xFF000000 + ADC_RX_BUFFER[BUFFER_SIDE_A][i];
		}
	}
	arm_q31_to_float((q31_t*)ADC_RX_BUFFER[BUFFER_SIDE_A], PROCESS_BUFFER[BUFFER_SIDE_A], AUDIO_BUFFER_LENGTH);
	rx_isr_count++;
	xTaskNotifyFromISR(process_audio_task_handle, PROCESS_SIDE_A, eSetBits, &xHigherPriorityTaskWoken);
	portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
}

// Rx complete callback occurs when BUFFER_SIDE_B (second row of the array) gets filled.
void HAL_SAI_RxCpltCallback(SAI_HandleTypeDef *hsai) {
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	for (uint32_t i = 0; i < AUDIO_BUFFER_LENGTH; i++) {
		if (ADC_RX_BUFFER[BUFFER_SIDE_B][i] & 0x800000) {
			ADC_RX_BUFFER[BUFFER_SIDE_B][i] = 0xFF000000 + ADC_RX_BUFFER[BUFFER_SIDE_B][i];
		}
	}
	arm_q31_to_float((q31_t*)ADC_RX_BUFFER[BUFFER_SIDE_B], PROCESS_BUFFER[BUFFER_SIDE_A], AUDIO_BUFFER_LENGTH);
	rx_isr_count++;
	xTaskNotifyFromISR(process_audio_task_handle, PROCESS_SIDE_B, eSetBits, &xHigherPriorityTaskWoken);
	portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
}
