/*
 * audio_buffers.c
 *
 * Copyright (c) 2018 Toni Naukkarinen <toni.naukkarinen@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "audio_buffers.h"
#include <string.h>

// Locate audio buffers to SDRAM
uint32_t ADC_RX_BUFFER[AUDIO_BUFFER_SIDES][AUDIO_BUFFER_LENGTH]		__attribute__((section(".sdram")));
uint32_t BT_RX_BUFFER[AUDIO_BUFFER_SIDES][AUDIO_BUFFER_LENGTH]		__attribute__((section(".sdram")));
q31_t OUTPUT_BUFFER[AUDIO_BUFFER_SIDES][AUDIO_BUFFER_LENGTH]		__attribute__((section(".sdram")));
q31_t OUTPUT_BUFFER_HIGH[AUDIO_BUFFER_SIDES][AUDIO_BUFFER_LENGTH]	__attribute__((section(".sdram")));
q31_t OUTPUT_BUFFER_LOW[AUDIO_BUFFER_SIDES][AUDIO_BUFFER_LENGTH]	__attribute__((section(".sdram")));
float32_t PROCESS_BUFFER_TMP[AUDIO_BUFFER_LENGTH]			__attribute__((section(".sdram")));
float32_t PROCESS_BUFFER[AUDIO_BUFFER_SIDES][AUDIO_BUFFER_LENGTH]	__attribute__((section(".sdram")));

uint16_t DAC_L_BUFFER[AUDIO_BUFFER_SIDES][DAC_BUFFER_LENGTH]		__attribute__((section(".sdram")));
uint16_t DAC_R_BUFFER[AUDIO_BUFFER_SIDES][DAC_BUFFER_LENGTH]		__attribute__((section(".sdram")));

void init_audio_buffers()
{
	// Zero out all the buffers.
	memset(ADC_RX_BUFFER,	0,	sizeof(uint32_t) * AUDIO_BUFFER_LENGTH * AUDIO_BUFFER_SIDES);
	memset(BT_RX_BUFFER,	0,	sizeof(uint32_t) * AUDIO_BUFFER_LENGTH * AUDIO_BUFFER_SIDES);
	memset(OUTPUT_BUFFER,	0,	sizeof(q31_t)	 * AUDIO_BUFFER_LENGTH * AUDIO_BUFFER_SIDES);
	memset(DAC_L_BUFFER,	2048,	sizeof(uint16_t) * DAC_BUFFER_LENGTH   * AUDIO_BUFFER_SIDES);
	memset(DAC_R_BUFFER,	2048,	sizeof(uint16_t) * DAC_BUFFER_LENGTH   * AUDIO_BUFFER_SIDES);
	memset(PROCESS_BUFFER,	0,	sizeof(float32_t)* AUDIO_BUFFER_LENGTH * AUDIO_BUFFER_SIDES);
	memset(OUTPUT_BUFFER_LOW,	0,	sizeof(q31_t)	 * AUDIO_BUFFER_LENGTH * AUDIO_BUFFER_SIDES);
	memset(OUTPUT_BUFFER_HIGH,	0,	sizeof(q31_t)	 * AUDIO_BUFFER_LENGTH * AUDIO_BUFFER_SIDES);
	memset(PROCESS_BUFFER_TMP,	0,	sizeof(float32_t)* AUDIO_BUFFER_LENGTH);
}
