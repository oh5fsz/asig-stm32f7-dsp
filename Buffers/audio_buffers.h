/*
 * audio_buffers.h
 *
 * Copyright (c) 2018 Toni Naukkarinen <toni.naukkarinen@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef AUDIO_BUFFERS_H_
#define AUDIO_BUFFERS_H_

#include <stdint.h>
#include "arm_math.h"

#define AUDIO_BUFFER_LENGTH	28672
// #define AUDIO_BUFFER_LENGTH	8192
#define DAC_BUFFER_LENGTH	AUDIO_BUFFER_LENGTH/2
#define AUDIO_BUFFER_SIDES	2

extern uint32_t ADC_RX_BUFFER[AUDIO_BUFFER_SIDES][AUDIO_BUFFER_LENGTH];
extern uint32_t BT_RX_BUFFER[AUDIO_BUFFER_SIDES][AUDIO_BUFFER_LENGTH];
extern q31_t OUTPUT_BUFFER[AUDIO_BUFFER_SIDES][AUDIO_BUFFER_LENGTH];
extern uint16_t DAC_L_BUFFER[AUDIO_BUFFER_SIDES][DAC_BUFFER_LENGTH];
extern uint16_t DAC_R_BUFFER[AUDIO_BUFFER_SIDES][DAC_BUFFER_LENGTH];
extern float32_t PROCESS_BUFFER[AUDIO_BUFFER_SIDES][AUDIO_BUFFER_LENGTH];

extern q31_t OUTPUT_BUFFER_LOW[AUDIO_BUFFER_SIDES][AUDIO_BUFFER_LENGTH];
extern q31_t OUTPUT_BUFFER_HIGH[AUDIO_BUFFER_SIDES][AUDIO_BUFFER_LENGTH];
extern float32_t PROCESS_BUFFER_TMP[AUDIO_BUFFER_LENGTH];
#endif /* AUDIO_BUFFERS_H_ */
