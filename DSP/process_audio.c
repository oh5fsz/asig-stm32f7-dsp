/*
 * process_audio.c
 *
 * Copyright (c) 2018 Toni Naukkarinen <toni.naukkarinen@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <defines.h>
#include <string.h>
#include "audio_buffers.h"
#include "limits.h"
#include "arm_math.h"
#include "FreeRTOS.h"
#include "task.h"
#include "externs.h"
#include "filter.h"
#include "output.h"
#include "main.h"

TaskHandle_t process_audio_task_handle;
uint32_t notify_value;
uint8_t output_enabled = 0;

// Default filters used: shelving bass and treble
filter_t* shelving_bass;
filter_t* shelving_treble;

filter_t* crossover_lowpass;
filter_t* crossover_highpass;

output_t* output_low;
output_t* output_high;
GPIO_PinState fault_pin;

// Pointers for 5 band equalizer
filter_t* equalizer[5];

static void update_filter_value(filter_t* filt, knob_t* knob)
{
	float tmp;
	int rc;
	rc = knob->get_scaled_value(knob, &tmp);
	if (rc == 0) {
		update_gain(filt, tmp);
	}
}

static void init_filters()
{
	init_filter_vtables();
	shelving_bass = new_filter(80.0f, SAMPLE_RATE, FILTER_Q, 1, SHELVING_BASS);
	shelving_treble = new_filter(6600.0f, SAMPLE_RATE, FILTER_Q, 1, SHELVING_TREBLE);
	crossover_lowpass = new_filter(1000.0f, SAMPLE_RATE, FILTER_Q, 0, LOWPASS);
	crossover_highpass = new_filter(1000.0f, SAMPLE_RATE, FILTER_Q, 0, HIGHPASS);

	equalizer[0] = new_filter(80.0f, SAMPLE_RATE, FILTER_Q, 0, PEAKING);
	equalizer[1] = new_filter(240.0f, SAMPLE_RATE, FILTER_Q, 0, PEAKING);
	equalizer[2] = new_filter(750.0f, SAMPLE_RATE, FILTER_Q, 0, PEAKING);
	equalizer[3] = new_filter(2200.0f, SAMPLE_RATE, FILTER_Q, 0, PEAKING);
	equalizer[4] = new_filter(6600.0f, SAMPLE_RATE, FILTER_Q, 0, PEAKING);
}

void process_audio_task(void *pvParams)
{
	init_filters();
	output_low = new_output((uint8_t)0x96, 0x6A);
	output_high = new_output((uint8_t)0x96, 0x6B);

	output_low->fault_pin_port = FAULT2_GPIO_Port;
	output_low->fault_pin = FAULT2_Pin;
	output_low->standby_pin_port = STANDBY2_GPIO_Port;
	output_low->standby_pin = STANDBY2_Pin;
	output_low->mute_pin_port = MUTE2_GPIO_Port;
	output_low->mute_pin = MUTE2_Pin;

	output_high->fault_pin_port = FAULT1_GPIO_Port;
	output_high->fault_pin = FAULT1_Pin;
	output_high->standby_pin_port = STANDBY1_GPIO_Port;
	output_high->standby_pin = STANDBY1_Pin;
	output_high->mute_pin_port = MUTE1_GPIO_Port;
	output_high->mute_pin = MUTE1_Pin;

	for (;;) {
		xTaskNotifyWait(0x00, ULONG_MAX, &notify_value, portMAX_DELAY);
		uint32_t event_group_value = xEventGroupGetBits(system_event_group);
		if ((notify_value & FILTER_UPDATE_REQUESTED) != 0) {

			// If 5 band EQ is not enabled, use shelving filters.
			if ((event_group_value & USE_5_BAND_EQ) == 0) {
				if ((event_group_value & DISABLE_SHELVING_BASS) == 0) {
					update_filter_value(shelving_bass, knob_array[BASS_CONTROL]);
				}
				if ((event_group_value & DISABLE_SHELVING_TREBLE) == 0) {
					update_filter_value(shelving_treble, knob_array[TREBLE_CONTROL]);
				}
			}
		}

		if ((notify_value & PROCESS_SIDE_A) != 0 || (notify_value & PROCESS_SIDE_B) != 0) {
			arm_scale_f32(PROCESS_BUFFER[BUFFER_SIDE_A], 0.08f, PROCESS_BUFFER[BUFFER_SIDE_A], AUDIO_BUFFER_LENGTH);
			if ((event_group_value & DISABLE_SHELVING_BASS) == 0 && (event_group_value & USE_5_BAND_EQ) == 0) {
				run_filter(shelving_bass, PROCESS_BUFFER[BUFFER_SIDE_A], PROCESS_BUFFER[BUFFER_SIDE_A], AUDIO_BUFFER_LENGTH);
			}
			if ((event_group_value & DISABLE_SHELVING_TREBLE) == 0 && (event_group_value & USE_5_BAND_EQ) == 0) {
				run_filter(shelving_treble, PROCESS_BUFFER[BUFFER_SIDE_A], PROCESS_BUFFER[BUFFER_SIDE_A], AUDIO_BUFFER_LENGTH);
			}

			if ((event_group_value & USE_5_BAND_EQ) != 0) {
				for (uint8_t i = 0; i < 5; i++) {
					run_filter(equalizer[i], PROCESS_BUFFER[BUFFER_SIDE_A], PROCESS_BUFFER[BUFFER_SIDE_A], AUDIO_BUFFER_LENGTH);
				}

			}
			run_filter(crossover_lowpass, PROCESS_BUFFER[BUFFER_SIDE_A], PROCESS_BUFFER[BUFFER_SIDE_B], AUDIO_BUFFER_LENGTH);
			run_filter(crossover_highpass, PROCESS_BUFFER[BUFFER_SIDE_A], PROCESS_BUFFER[BUFFER_SIDE_A], AUDIO_BUFFER_LENGTH);
		}

		if ((notify_value & PROCESS_SIDE_A) != 0) {
			arm_float_to_q31(PROCESS_BUFFER_TMP, OUTPUT_BUFFER[BUFFER_SIDE_A], AUDIO_BUFFER_LENGTH);
			arm_float_to_q31(PROCESS_BUFFER[BUFFER_SIDE_B], (q31_t*)OUTPUT_BUFFER_LOW[BUFFER_SIDE_A], AUDIO_BUFFER_LENGTH);
			arm_float_to_q31(PROCESS_BUFFER[BUFFER_SIDE_A], (q31_t*)OUTPUT_BUFFER_HIGH[BUFFER_SIDE_A], AUDIO_BUFFER_LENGTH);

			if (!output_enabled) {
				output_enabled = 1;
				HAL_SAI_Transmit_DMA(&hsai_BlockB2, (uint8_t*)*OUTPUT_BUFFER_LOW, (uint16_t)(AUDIO_BUFFER_LENGTH * AUDIO_BUFFER_SIDES));
				HAL_SAI_Transmit_DMA(&hsai_BlockA2, (uint8_t*)*OUTPUT_BUFFER_HIGH, (uint16_t)(AUDIO_BUFFER_LENGTH * AUDIO_BUFFER_SIDES));
				output_low->start(output_low);
				output_high->start(output_high);
			}

			xTaskNotify(dac_task_handle, PROCESS_SIDE_A, eSetBits);
		}
		else if ((notify_value & PROCESS_SIDE_B) != 0) {
			arm_float_to_q31(PROCESS_BUFFER_TMP, OUTPUT_BUFFER[BUFFER_SIDE_B], AUDIO_BUFFER_LENGTH);
			arm_float_to_q31(PROCESS_BUFFER[BUFFER_SIDE_B], (q31_t*)OUTPUT_BUFFER_LOW[BUFFER_SIDE_B], AUDIO_BUFFER_LENGTH);
			arm_float_to_q31(PROCESS_BUFFER[BUFFER_SIDE_A], (q31_t*)OUTPUT_BUFFER_HIGH[BUFFER_SIDE_B], AUDIO_BUFFER_LENGTH);
			output_low->get_status(output_low);
			output_high->get_status(output_high);
			xTaskNotify(dac_task_handle, PROCESS_SIDE_B, eSetBits);
		}
	}
}
