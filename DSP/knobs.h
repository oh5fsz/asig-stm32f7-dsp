/*
 * knobs.h
 *
 * Copyright (c) 2018 Toni Naukkarinen <toni.naukkarinen@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef KNOBS_H_
#define KNOBS_H_

#include "filter.h"
#include <stdint.h>
#include "FreeRTOS.h"
#include "semphr.h"

// ADC min/max values. STM32F7 ADC is 12bit.
#define ADC_MIN		0
#define ADC_MAX		4095

typedef struct knob knob_t;

struct knob
{
	uint16_t adc_value;
	float mapped_value;
	filter_t* mapped_filter;
	SemaphoreHandle_t knob_semaphore;

	BaseType_t (*set_value)(knob_t*, uint16_t);
	float (*scale_for_filter)(knob_t* self, float, float);
	void (*map_filter_to_knob)(knob_t*, filter_t*);
	int (*get_scaled_value)(knob_t*, float*);
};

float scale_for_filter(knob_t* self, float, float);
void map_filter_to_knob(knob_t* self, filter_t* filter);
void init_knobs();

#endif /* KNOBS_H_ */
