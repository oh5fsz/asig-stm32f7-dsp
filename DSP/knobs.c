/*
 * knobs.c
 *
 * Copyright (c) 2018 Toni Naukkarinen <toni.naukkarinen@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "externs.h"
#include "knobs.h"
#include "stm32f7xx_hal.h"
#include "task.h"
#include <assert.h>
#include <defines.h>

static int get_scaled_value(knob_t* self, float* buf);

uint16_t adc_buffer[3];
knob_t* knob_array[3];
volatile uint32_t adc_conversion_count;

/* Sets the adc_value of the knob after taking a semaphore.
 * Value won't get updated if semaphore canno't be taken.
 * It's not the end of the world if that happens.
 */
static BaseType_t set_value(knob_t* self, uint16_t value)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	if (xSemaphoreTakeFromISR(self->knob_semaphore, &xHigherPriorityTaskWoken) == pdTRUE) {
		self->adc_value = value;
		xSemaphoreGiveFromISR(self->knob_semaphore, &xHigherPriorityTaskWoken);
	}
	return xHigherPriorityTaskWoken;
}

// Creates a new knob object.
static knob_t* new_knob()
{
	knob_t* new_knob = (knob_t*)pvPortMalloc(sizeof(knob_t));
	assert(new_knob != NULL);
	new_knob->adc_value = 0;
	new_knob->mapped_value = 0.0f;
	new_knob->knob_semaphore = xSemaphoreCreateBinary();
	new_knob->set_value = &set_value;
	new_knob->scale_for_filter = &scale_for_filter;
	new_knob->get_scaled_value = &get_scaled_value;
	// Knob is ready. Free the semaphore.
	xSemaphoreGive(new_knob->knob_semaphore);
	return new_knob;
}

void init_knobs()
{
	knob_array[TREBLE_CONTROL] = new_knob();
	knob_array[BASS_CONTROL] = new_knob();
	knob_array[VOLUME_CONTROL] = new_knob();

	// ADC is set up to continuously read the first three channels.
	HAL_ADC_Start_DMA(&hadc1, (uint32_t*)adc_buffer, 3);
}

static int get_scaled_value(knob_t* self, float* buf)
{
	int rc = -1;

	if (xSemaphoreTake(self->knob_semaphore, 0) == pdTRUE) {
		*buf = self->mapped_value;
		xSemaphoreGive(self->knob_semaphore);
		rc = 0;
	}
	return rc;
}

// Scales the raw ADC value to gain value usable in filters/volume.
float scale_for_filter(knob_t* self, float filter_min, float filter_max)
{
	float raw_value = (float)self->adc_value;
	float output = filter_min+((filter_max-filter_min)/(ADC_MAX-ADC_MIN))*(raw_value-filter_min);
	self->mapped_value = output;
	return output;
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc)
{
	uint32_t sys_config = xEventGroupGetBitsFromISR(system_event_group);
	if ((sys_config & DISABLE_FILTER_UPDATE) == 0) {
		for (uint8_t i = 0; i < 2; i++) {
			if (knob_array[i]->adc_value != adc_buffer[i]) {
				knob_array[i]->set_value(knob_array[i], adc_buffer[i]);
				knob_array[i]->scale_for_filter(knob_array[i], -12.0f, 12.0f);
			}
		}

		knob_array[2]->set_value(knob_array[2], adc_buffer[2]);
		knob_array[2]->scale_for_filter(knob_array[2], 0.0f, 255.0f);
		output_low->set_volume(output_low, (uint8_t)knob_array[2]->mapped_value);
		output_high->set_volume(output_high, (uint8_t)knob_array[2]->mapped_value);
		BaseType_t xHigherPriorityTaskWoken = pdFALSE;
		adc_conversion_count++;
		xTaskNotifyFromISR(process_audio_task_handle, FILTER_UPDATE_REQUESTED, eSetBits, &xHigherPriorityTaskWoken);
		portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
	}
}
