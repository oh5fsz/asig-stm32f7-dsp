/*
 * filter.h
 *
 * Copyright (c) 2018 Toni Naukkarinen <toni.naukkarinen@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef FILTER_H_
#define FILTER_H_

#define SAMPLE_RATE	48000.0f
#define FILTER_Q	0.7071f

#include "FreeRTOS.h"
#include "semphr.h"
#include "arm_math.h"

typedef enum
{
	SHELVING_BASS,
	SHELVING_TREBLE,
	PEAKING,
	LOWPASS,
	HIGHPASS
} filter_type_t;

typedef struct filter filter_t;
typedef struct filter_vtable filter_vtable_t;

struct filter
{
	float32_t fc;
	float32_t fs;
	float32_t Q;
	float32_t G;

	/* Coefficient order in ARM filter object:
	* {b10, b11, b12, a11, a12}
	* Direct Form II Transposed requires a 2-value state buffer.
	*
	* Check CMSIS-DSP documentation.
	*/

	float32_t state_buffer[2];
	float32_t coefficients[5];
	SemaphoreHandle_t semaphore;
	filter_type_t type;
	arm_biquad_cascade_df2T_instance_f32 arm_filter;

	filter_vtable_t* vptr;
};

// Use vtable for now. There might be additional functions in the future.
struct filter_vtable
{
	void (*calculate)(filter_t*);
	void (*run)(filter_t*, float32_t*, float32_t*, uint32_t);
	void (*update_coefficients)(filter_t*, float32_t*, float32_t*);
};

filter_t *new_filter(float32_t fc, float32_t fs, float32_t Q, float32_t G, filter_type_t type);
void calculate_filter(filter_t* filt);
int update_gain(filter_t* filt, float32_t G);
int update_center_frequency(filter_t* filt, float32_t fc);
void run_filter(filter_t* filt, float32_t* src, float32_t* dst, uint32_t length);
void init_filter_vtables();

#endif /* FILTER_H_ */
