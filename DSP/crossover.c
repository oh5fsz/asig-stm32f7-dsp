/*
 * crossover.c
 *
 *  Created on: Mar 22, 2018
 *      Author: tmn
 */

#include "arm_math.h"
#include "math.h"
#include "filter.h"

filter_vtable_t crossover_vtable;

static void calculate_crossover_filter(filter_t* self)
{
	float32_t b[3];
	float32_t a[3];

	float32_t w0 = 2 * M_PI * (self->fc/self->fs);
	float32_t alpha = sin(w0)/(2*self->Q);
	float32_t cos_w0 = arm_cos_f32(w0);

	switch (self->type) {
	case LOWPASS:
		b[0] =  (1 - cos_w0)/2;
		b[1] =   1 - cos_w0;
		b[2] =  (1 - cos_w0)/2;
		a[0] =   1 + alpha;
		a[1] =  -2*cos_w0;
		a[2] =   1 - alpha;
		break;
	case HIGHPASS:
		b[0] =  (1 + cos_w0)/2;
		b[1] = -(1 + cos_w0);
		b[2] =  (1 + cos_w0)/2;
		a[0] =   1 + alpha;
		a[1] =  -2*cos_w0;
		a[2] =   1 - alpha;
		break;
	case SHELVING_BASS:
		break;
	case SHELVING_TREBLE:
		break;
	case PEAKING:
		break;
	}

	self->vptr->update_coefficients(self, b, a);
}

static void run_crossover_filter(filter_t* self, float_t* src, float_t* dst, uint32_t length)
{
	arm_biquad_cascade_df2T_f32(&self->arm_filter, src, dst, length);
}


void init_crossover_vtable()
{
	crossover_vtable.calculate = &calculate_crossover_filter;
	crossover_vtable.run = &run_crossover_filter;
}
