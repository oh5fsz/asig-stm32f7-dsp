/*
 * filter.c
 *
 * Copyright (c) 2018 Toni Naukkarinen <toni.naukkarinen@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <assert.h>
#include "FreeRTOS.h"
#include "semphr.h"
#include "filter.h"
#include "shelving.h"
#include "peaking.h"
#include "crossover.h"

extern filter_vtable_t shelving_vtable;
extern filter_vtable_t peaking_vtable;
extern filter_vtable_t crossover_vtable;

static void update_coefficients(filter_t* self, float32_t *b, float32_t *a);

void init_filter_vtables()
{
	init_shelving_vtable();
	init_peaking_vtable();
	init_crossover_vtable();
}

filter_t *new_filter(float32_t fc, float32_t fs, float32_t Q, float32_t G, filter_type_t type)
{
	filter_t* filt = (filter_t*)pvPortMalloc(sizeof(filter_t));
	assert(filt != 0);

	switch (type) {
	case SHELVING_BASS:
		filt->vptr = &shelving_vtable;
		break;
	case SHELVING_TREBLE:
		filt->vptr = &shelving_vtable;
		break;
	case PEAKING:
		filt->vptr = &peaking_vtable;
		break;
	case LOWPASS:
		filt->vptr = &crossover_vtable;
		break;
	case HIGHPASS:
		filt->vptr = &crossover_vtable;
		break;
	}

	filt->vptr->update_coefficients = &update_coefficients;

	filt->type = type;
	filt->fc = fc;
	filt->fs = SAMPLE_RATE;
	filt->Q = Q;
	filt->G = G;
	filt->vptr->calculate(filt);
	arm_biquad_cascade_df2T_init_f32(&filt->arm_filter, 1, &filt->coefficients[0], &filt->state_buffer[0]);
	filt->semaphore = xSemaphoreCreateMutex();
	xSemaphoreGive(filt->semaphore);
	return filt;
}

static void update_coefficients(filter_t* self, float32_t *b, float32_t *a)
{
	// Normalize with a1 and save to coefficients table.
	// CMSIS wants a1 and a2 inverted.
	self->coefficients[0] = b[0] / a[0];
	self->coefficients[1] = b[1] / a[0];
	self->coefficients[2] = b[2] / a[0];
	self->coefficients[3] = -(a[1] / a[0]);
	self->coefficients[4] = -(a[2] / a[0]);
}

int update_gain(filter_t* filt, float32_t G)
{
	int rc = 2;
	if (G <= 24.00f) {
		if (xSemaphoreTake(filt->semaphore, 100) == pdTRUE) {
			filt->G = G;
			filt->vptr->calculate(filt);
			xSemaphoreGive(filt->semaphore);
			rc = 1;
		}
	}
	return rc;
}

int update_center_frequency(filter_t* filt, float32_t fc)
{
	int rc = 2;
	if (fc > 0.0f && fc < 22000.0f) {
		if (xSemaphoreTake(filt->semaphore, 100) == pdTRUE) {
			filt->fc = fc;
			filt->vptr->calculate(filt);
			xSemaphoreGive(filt->semaphore);
			rc = 1;
		}
	}
	return rc;
}

void calculate_filter(filter_t* filt)
{
	if (xSemaphoreTake(filt->semaphore, 100) == pdTRUE) {
		filt->vptr->calculate(filt);
		xSemaphoreGive(filt->semaphore);
	}
}

void run_filter(filter_t* filt, float32_t* src, float32_t* dst, uint32_t length)
{
	if (xSemaphoreTake(filt->semaphore, 0) == pdTRUE) {
		filt->vptr->run(filt, src, dst, length);
		xSemaphoreGive(filt->semaphore);
	}
}
