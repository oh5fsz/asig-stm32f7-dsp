/*
 * shelving.c
 *
 * Copyright (c) 2018 Toni Naukkarinen <toni.naukkarinen@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "FreeRTOS.h"
#include "math.h"
#include <stdint.h>
#include <assert.h>
#include "arm_math.h"
#include "audio_buffers.h"
#include "filter.h"

filter_vtable_t shelving_vtable;

static void run_shelving_filter(filter_t* self, float32_t* src, float32_t* dst, uint32_t length);
static void calculate_shelving_filter(filter_t* self);

void init_shelving_vtable()
{
	shelving_vtable.calculate = &calculate_shelving_filter;
	shelving_vtable.run = &run_shelving_filter;
}

static void run_shelving_filter(filter_t* self, float32_t* src, float32_t* dst, uint32_t length)
{
	arm_biquad_cascade_df2T_f32(&self->arm_filter, src, dst, length);
}

static void calculate_shelving_filter(filter_t* self)
{
	float32_t a[3];
	float32_t b[3];

	float32_t A = pow(10, self->G / 40.0);
	float32_t w0 = 2 * M_PI * (self->fc / self->fs);

	float32_t alpha = (sin(w0) / 2) * sqrt(2);

	switch (self->type) {
	case SHELVING_BASS:
		b[0] = A * ((A + 1) - (A - 1) * cos(w0) + 2 * sqrt(A) * alpha);
		b[1] = 2 * A * ((A - 1) - (A + 1) * cos(w0));
		b[2] = A * ((A + 1) - (A - 1) * cos(w0) - 2 * sqrt(A) * alpha);
		a[0] = (A + 1) + (A - 1) * cos(w0) + 2 * sqrt(A) * alpha;
		a[1] = -2 * ((A - 1) + (A + 1) * cos(w0));
		a[2] = (A + 1) + (A - 1) * cos(w0) - 2 * sqrt(A) * alpha;
		break;
	case SHELVING_TREBLE:
		b[0] =    A * ( (A + 1) + (A - 1) * cos(w0) + 2 * sqrt(A) * alpha );
		b[1] = -2 * A * ( (A - 1) + (A + 1) * cos(w0) );
		b[2] =    A * ( (A + 1) + (A - 1) * cos(w0) - 2 * sqrt(A) * alpha );
		a[0] =        (A + 1) - (A - 1) * cos(w0) + 2 * sqrt(A) * alpha;
		a[1] =    2 * ( (A - 1) - (A + 1) * cos(w0));
		a[2] =        (A + 1) - (A - 1) * cos(w0) - 2 * sqrt(A) * alpha;
		break;
	case PEAKING:
		// Peaking filter calculation is not done in this file.
		break;
	case HIGHPASS:
		break;
	case LOWPASS:
		break;
	}

	self->vptr->update_coefficients(self, b, a);
}
