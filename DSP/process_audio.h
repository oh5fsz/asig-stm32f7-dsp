/*
 * process_audio.h
 *
 *  Created on: Mar 19, 2018
 *      Author: tmn
 */

#ifndef PROCESS_AUDIO_H_
#define PROCESS_AUDIO_H_

void process_audio_task(void *pvParams);

#endif /* PROCESS_AUDIO_H_ */
