/*
 * peaking.c
 *
 * Copyright (c) 2018 Toni Naukkarinen <toni.naukkarinen@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "filter.h"
#include "math.h"
#include "arm_math.h"

filter_vtable_t peaking_vtable;

static void calculate_peaking_filter(filter_t* self);
static void run_peaking_filter(filter_t* self, float_t* src, float_t* dst, uint32_t length);

void init_peaking_vtable()
{
	peaking_vtable.calculate = &calculate_peaking_filter;
	peaking_vtable.run = &run_peaking_filter;
}

static void calculate_peaking_filter(filter_t* self)
{
	float32_t A = pow(10, self->G/40.0);
	float32_t w0 = 2 * M_PI * self->fc/self->fs;
	float32_t alpha = arm_sin_f32(w0/2*self->Q);

	float32_t b[3], a[3];

	b[0] = 1 + alpha * A;
	b[1] = -2 * arm_cos_f32(w0);
	b[2] = 1 - alpha*A;
	a[0] = 1 + alpha/A;
	a[1] = -2 * arm_cos_f32(w0);
	a[2] = 1 - alpha/A;

	self->vptr->update_coefficients(self, b, a);
}

static void run_peaking_filter(filter_t* self, float_t* src, float_t* dst, uint32_t length)
{
	arm_biquad_cascade_df2T_f32(&self->arm_filter, src, dst, length);
}
