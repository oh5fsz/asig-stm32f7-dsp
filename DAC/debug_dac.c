/*
 * debug_dac.c
 *
 * Copyright (c) 2018 Toni Naukkarinen <toni.naukkarinen@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <defines.h>
#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"
#include "audio_buffers.h"
#include "stm32f7xx_hal.h"
#include <limits.h>
#include "externs.h"
#include "debug_dac.h"

TaskHandle_t dac_task_handle;

uint32_t notify_value;
uint32_t left_count = 0;
uint32_t right_count = 0;
uint32_t dac_process_count = 0;
uint8_t dac_enabled = 0;

void dac_enable()
{
	HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t*)DAC_L_BUFFER, (uint16_t)DAC_BUFFER_LENGTH * (uint16_t)AUDIO_BUFFER_SIDES, DAC_ALIGN_12B_R);
	HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_2, (uint32_t*)DAC_R_BUFFER, (uint16_t)DAC_BUFFER_LENGTH * (uint16_t)AUDIO_BUFFER_SIDES, DAC_ALIGN_12B_R);
	HAL_TIM_Base_Start_IT(&htim6);
	dac_enabled = 1;
}

void dac_disable()
{
	HAL_DAC_Stop_DMA(&hdac, DAC_CHANNEL_1);
	HAL_DAC_Stop_DMA(&hdac, DAC_CHANNEL_2);
	HAL_TIM_Base_Stop(&htim6);
	dac_enabled = 0;
}

void dac_process_task(void *pvParams)
{
	for (;;) {
		xTaskNotifyWait(0x00, ULONG_MAX, &notify_value, portMAX_DELAY);
		uint32_t event_group_bits = xEventGroupGetBits(system_event_group);
		if ((notify_value & PROCESS_SIDE_A) != 0) {
			left_count = 0;
			right_count = 0;
			for (uint32_t i = 0; i < AUDIO_BUFFER_LENGTH; i++) {
				if (i % 2 == 0) {
					DAC_L_BUFFER[BUFFER_SIDE_A][left_count] = (uint16_t)((uint32_t)OUTPUT_BUFFER[BUFFER_SIDE_A][i] >> 12) + 2048;
					left_count++;
				}
				else {
					DAC_R_BUFFER[BUFFER_SIDE_A][right_count] = (uint16_t)((uint32_t)OUTPUT_BUFFER[BUFFER_SIDE_A][i] >> 12) + 2048;
					right_count++;
				}
			}
		}
		else if ((notify_value & PROCESS_SIDE_B) != 0) {
			left_count = 0;
			right_count = 0;
			for (uint32_t i = 0; i < AUDIO_BUFFER_LENGTH; i++) {
				if (i % 2 == 0) {
					DAC_L_BUFFER[BUFFER_SIDE_B][left_count] = (uint16_t)((uint32_t)OUTPUT_BUFFER[BUFFER_SIDE_B][i] >> 12) + 2048;
					left_count++;
				}
				else {
					DAC_R_BUFFER[BUFFER_SIDE_B][right_count] = (uint16_t)((uint32_t)OUTPUT_BUFFER[BUFFER_SIDE_B][i] >> 12) + 2048;
					right_count++;
				}
			}
			if ((event_group_bits & DAC_ENABLE_BIT) != 0 && dac_enabled == 0) {
				dac_enable();
			}
			else if ((event_group_bits & DAC_ENABLE_BIT) == 0 && dac_enabled == 1) {
				dac_disable();
			}
		}
		if (hdac.ErrorCode != 0 && dac_enabled == 1) {
			dac_disable();
			dac_enable();
		}
	}
}
