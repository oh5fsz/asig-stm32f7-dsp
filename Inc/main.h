/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define ADC_MCLK_Pin GPIO_PIN_2
#define ADC_MCLK_GPIO_Port GPIOE
#define ADC_DATA_Pin GPIO_PIN_3
#define ADC_DATA_GPIO_Port GPIOE
#define ADC_WORD_SELECT_Pin GPIO_PIN_4
#define ADC_WORD_SELECT_GPIO_Port GPIOE
#define ADC_SCK_Pin GPIO_PIN_5
#define ADC_SCK_GPIO_Port GPIOE
#define BLUETOOTH_AUDIODATA_Pin GPIO_PIN_6
#define BLUETOOTH_AUDIODATA_GPIO_Port GPIOE
#define BLUETOOTH_MCLK_Pin GPIO_PIN_7
#define BLUETOOTH_MCLK_GPIO_Port GPIOF
#define BLUETOOTH_SCK_Pin GPIO_PIN_8
#define BLUETOOTH_SCK_GPIO_Port GPIOF
#define BLUETOOTH_WORD_SELECT_Pin GPIO_PIN_9
#define BLUETOOTH_WORD_SELECT_GPIO_Port GPIOF
#define TREBLE_CONTROL_Pin GPIO_PIN_1
#define TREBLE_CONTROL_GPIO_Port GPIOA
#define BASS_CONTROL_Pin GPIO_PIN_2
#define BASS_CONTROL_GPIO_Port GPIOA
#define VOLUME_CONTROL_Pin GPIO_PIN_3
#define VOLUME_CONTROL_GPIO_Port GPIOA
#define LEFT_CHANNEL_ANALOG_OUT_Pin GPIO_PIN_4
#define LEFT_CHANNEL_ANALOG_OUT_GPIO_Port GPIOA
#define RIGHT_CHANNEL_ANALOG_OUT_Pin GPIO_PIN_5
#define RIGHT_CHANNEL_ANALOG_OUT_GPIO_Port GPIOA
#define OUTPUT_DATA_A_Pin GPIO_PIN_11
#define OUTPUT_DATA_A_GPIO_Port GPIOD
#define OUTPUT_WORD_SELECT_Pin GPIO_PIN_12
#define OUTPUT_WORD_SELECT_GPIO_Port GPIOD
#define OUTPUT_SCK_Pin GPIO_PIN_13
#define OUTPUT_SCK_GPIO_Port GPIOD
#define DEBUG_UART_TX_Pin GPIO_PIN_9
#define DEBUG_UART_TX_GPIO_Port GPIOA
#define DEBUG_UART_RX_Pin GPIO_PIN_10
#define DEBUG_UART_RX_GPIO_Port GPIOA
#define BLUETOOTH_UART_TX_Pin GPIO_PIN_12
#define BLUETOOTH_UART_TX_GPIO_Port GPIOC
#define BLUETOOTH_UART_RX_Pin GPIO_PIN_2
#define BLUETOOTH_UART_RX_GPIO_Port GPIOD
#define BLUETOOTH_TX_IND_Pin GPIO_PIN_3
#define BLUETOOTH_TX_IND_GPIO_Port GPIOD
#define BLUETOOTH_RX_IND_Pin GPIO_PIN_4
#define BLUETOOTH_RX_IND_GPIO_Port GPIOD
#define MUTE1_Pin GPIO_PIN_5
#define MUTE1_GPIO_Port GPIOD
#define STANDBY1_Pin GPIO_PIN_6
#define STANDBY1_GPIO_Port GPIOD
#define MUTE2_Pin GPIO_PIN_7
#define MUTE2_GPIO_Port GPIOD
#define STANDBY2_Pin GPIO_PIN_9
#define STANDBY2_GPIO_Port GPIOG
#define OUTPUT_DATA_B_Pin GPIO_PIN_10
#define OUTPUT_DATA_B_GPIO_Port GPIOG
#define WARN1_Pin GPIO_PIN_11
#define WARN1_GPIO_Port GPIOG
#define FAULT1_Pin GPIO_PIN_12
#define FAULT1_GPIO_Port GPIOG
#define WARN2_Pin GPIO_PIN_13
#define WARN2_GPIO_Port GPIOG
#define FAULT2_Pin GPIO_PIN_14
#define FAULT2_GPIO_Port GPIOG
#define FAULT2_EXTI_IRQn EXTI15_10_IRQn
#define OUTPUT_MCLK_Pin GPIO_PIN_4
#define OUTPUT_MCLK_GPIO_Port GPIOI

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
