/*
 * output.h
 *
 *  Created on: Apr 1, 2018
 *      Author: tmn
 */

#ifndef OUTPUT_H_
#define OUTPUT_H_

// TAS6422-Q1 register definitions
#define TAS6422_MODE_CONTROL		0x00
#define TAS6422_MISC_CONTROL_1		0x01
#define TAS6422_MISC_CONTROL_2		0x02
#define TAS6422_SAP_CONTROL		0x03
#define TAS6422_CHANNEL_CONTROL		0x04
#define TAS6422_CH1_VOLUME_CONTROL	0x05
#define TAS6422_CH2_VOLUME_CONTROL	0x06
#define TAS6422_DIAG_CONTROL_1		0x09
#define TAS6422_DIAG_CONTROL_2		0x0A
#define TAS6422_DC_LOAD_DIAG_REPORT_1	0x0C
#define TAS6422_DC_LOAD_DIAG_REPORT_3	0x0E
#define TAS6422_CH_STATE_REPORT		0x0F
#define TAS6422_CHANNEL_FAULTS		0x10
#define TAS6422_GLOBAL_FAULTS_1		0x11
#define TAS6422_GLOBAL_FAULTS_2		0x12
#define TAS6422_WARNINGS		0x13
#define TAS6422_PIN_CONTROL		0x14
#define TAS6422_AC_LOAD_DIAG_CONTROL_1	0x15
#define TAS6422_AC_LOAD_DIAG_CONTROL_2	0x16
#define TAS6422_AC_LOAD_DIAG_REPORT_CH1	0x17
#define TAS6422_AC_LOAD_DIAG_REPORT_CH2	0x18
#define TAS6422_AC_LOAD_DIAG_PHASE_HI	0x1B
#define TAS6422_AC_LOAD_DIAG_PHASE_LO	0x1C
#define TAS6422_AC_LOAD_DIAG_STI_HI	0x1D
#define TAS6422_AC_LOAD_DIAG_STI_LO	0x1E
#define TAS6422_MISC_CONTROL_3		0x21
#define TAS6422_CLIP_CONTROL		0x22
#define TAS6422_CLIP_WINDOW		0x23
#define TAS6422_CLIP_WARNING		0x24
#define TAS6422_ILIMIT_STATUS		0x25
#define TAS6422_MISC_CONTROL_4		0x26


typedef struct output output_t;
typedef struct tas6422_registers tas6422_registers_t;

struct tas6422_registers
{
	uint8_t ch_state;
	uint8_t sap_control;
	uint8_t dc_load_diag;
	uint8_t global_faults1;
	uint8_t global_faults2;
	uint8_t warnings;
	uint8_t ch_faults;
	uint8_t ch1_volume;
	uint8_t ch2_volume;
	GPIO_PinState fault_pin_state;
};

struct output
{
	uint8_t volume;
	uint8_t address;
	uint8_t read_address;

	GPIO_TypeDef* fault_pin_port;
	uint16_t fault_pin;
	GPIO_TypeDef* standby_pin_port;
	uint16_t standby_pin;
	GPIO_TypeDef* mute_pin_port;
	uint16_t mute_pin;

	SAI_HandleTypeDef* sai;
	uint32_t* data_buffer;

	tas6422_registers_t status_regs;

	void (*start)(output_t*);
	void (*stop)(output_t*);
	void (*set_volume)(output_t*, uint8_t);
	void (*get_status)(output_t*);
};


void init_outputs();
output_t* new_output(uint8_t volume, uint8_t device_address);

#endif /* OUTPUT_H_ */
