/*
 * output.c
 *
 *  Created on: Apr 1, 2018
 *      Author: tmn
 */

#include <assert.h>
#include "externs.h"
#include "output.h"
#include "stm32f7xx_hal.h"
#include "audio_buffers.h"

uint8_t result;
uint8_t data[2];

static void tas6422_set_volume(output_t* self, uint8_t volume)
{
	self->volume = volume;
	HAL_I2C_Mem_Write(&hi2c1, self->address, TAS6422_CH1_VOLUME_CONTROL, 1, &volume, 1, 100);
	HAL_I2C_Mem_Write(&hi2c1, self->address, TAS6422_CH2_VOLUME_CONTROL, 1, &volume, 1, 100);
}

static void i2c_error_callback()
{

}

static void tas6422_start(output_t* self)
{
	HAL_StatusTypeDef status;
	// Assert STANDBY and MUTE pins.
	HAL_GPIO_WritePin(self->standby_pin_port, self->standby_pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(self->mute_pin_port, self->mute_pin, GPIO_PIN_SET);

	uint8_t reg_value = 0x80;
	status = HAL_I2C_Mem_Write(&hi2c1, self->address, TAS6422_MODE_CONTROL, 1, &reg_value, 1, 100);

	if (status != HAL_OK) {
		i2c_error_callback();
	}

	reg_value = 0x7D;
	status = HAL_I2C_Mem_Write(&hi2c1, self->address, TAS6422_MISC_CONTROL_1, 1, &reg_value, 1, 100);

	if (status != HAL_OK) {
		i2c_error_callback();
	}

	reg_value = 0x44;
	HAL_I2C_Mem_Write(&hi2c1, self->address, TAS6422_SAP_CONTROL, 1, &reg_value, 1, 100);

	reg_value = 0x78;
	status = HAL_I2C_Mem_Write(&hi2c1, self->address, TAS6422_CH1_VOLUME_CONTROL, 1, &reg_value, 1, 100);

	if (status != HAL_OK) {
		i2c_error_callback();
	}

	status = HAL_I2C_Mem_Write(&hi2c1, self->address, TAS6422_CH2_VOLUME_CONTROL, 1, &reg_value, 1, 100);

	if (status != HAL_OK) {
		i2c_error_callback();
	}

	reg_value = 0x80;
	status = HAL_I2C_Mem_Write(&hi2c1, self->address, TAS6422_MISC_CONTROL_3, 1, &reg_value, 1, 100);

	if (status != HAL_OK) {
		i2c_error_callback();
	}

	reg_value = 0x05;
	status = HAL_I2C_Mem_Write(&hi2c1, self->address, TAS6422_CHANNEL_CONTROL, 1, &reg_value, 1, 100);

	if (status != HAL_OK) {
		i2c_error_callback();
	}

	if (status != HAL_OK) {
		i2c_error_callback();
	}

}

static void tas6422_get_status(output_t* self)
{
	// Clear all faults

	// vTaskDelay(100);

	HAL_I2C_Mem_Read(&hi2c1, self->address, TAS6422_CH_STATE_REPORT, 1, &self->status_regs.ch_state, 1, 100);
	HAL_I2C_Mem_Read(&hi2c1, self->address, TAS6422_SAP_CONTROL, 1, &self->status_regs.sap_control, 1, 100);
	HAL_I2C_Mem_Read(&hi2c1, self->address, TAS6422_DC_LOAD_DIAG_REPORT_1, 1, &self->status_regs.dc_load_diag, 1, 100);
	HAL_I2C_Mem_Read(&hi2c1, self->address, TAS6422_GLOBAL_FAULTS_1, 1, &self->status_regs.global_faults1, 1, 100);
	HAL_I2C_Mem_Read(&hi2c1, self->address, TAS6422_GLOBAL_FAULTS_2, 1, &self->status_regs.global_faults2, 1, 100);
	HAL_I2C_Mem_Read(&hi2c1, self->address, TAS6422_WARNINGS, 1, &self->status_regs.warnings, 1, 100);
	HAL_I2C_Mem_Read(&hi2c1, self->address, TAS6422_CHANNEL_FAULTS, 1, &self->status_regs.ch_faults, 1, 100);
	HAL_I2C_Mem_Read(&hi2c1, self->address, TAS6422_CH1_VOLUME_CONTROL, 1, &self->status_regs.ch1_volume, 1, 100);
	HAL_I2C_Mem_Read(&hi2c1, self->address, TAS6422_CH2_VOLUME_CONTROL, 1, &self->status_regs.ch2_volume, 1, 100);

	if (self->status_regs.ch_faults != 0) {
		uint8_t reg_value = 0x05;
		HAL_I2C_Mem_Write(&hi2c1, self->address, TAS6422_CHANNEL_CONTROL, 1, &reg_value, 1, 100);
	}
	self->status_regs.fault_pin_state = HAL_GPIO_ReadPin(self->fault_pin_port, self->fault_pin);
}

static void tas6422_stop(output_t* self)
{
	HAL_SAI_DMAStop(self->sai);
}

output_t* new_output(uint8_t volume, uint8_t device_address)
{
	output_t* o = (output_t*)pvPortMalloc(sizeof(output_t));
	assert(o != 0);

	o->address = (device_address << 1);
	o->set_volume = &tas6422_set_volume;
	o->start = &tas6422_start;
	o->stop = &tas6422_stop;
	o->get_status = &tas6422_get_status;

	return o;
}
